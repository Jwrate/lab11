#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Sets the value of size to be the number of valid
// entries in the array (not the total array length).
char ** loadDictionary(char *filename, int *size)
{
	FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open dictionary");
	    exit(1);
	}

	// TODO
	// Allocate memory for an array of strings (arr).
    char **dictionary;
  	int numWords = 100;
    int count = 0;
  	dictionary = (char*)malloc(numWords*sizeof(char*));
	// Read the dictionary line by line.
  	char line[1024];
  
  	while( fscanf(in,"%s",line)>0)
    {
      // Expand array if necessary (realloc).
      if(count==numWords)
      {
        numWords += 100;
        dictionary = (char*)realloc(dictionary,numWords*sizeof(char*));
      }

      // Allocate memory for the string (str).
      char *str = (char*)malloc(strlen(line)*sizeof(char));

      // Copy each line into the string (use strcpy).
      strcpy(str,line);
      // Attach the string to the large array (assignment =).
	  dictionary[count++]=str;
    }  
      
	// The size should be the number of entries in the array.
	*size = count;
	
	// Return pointer to the array of strings.
	return dictionary;
}

// Search the dictionary for the target string
// Return the found string or NULL if not found.
char * searchDictionary(char *target, char **dictionary, int size)
{
    if (dictionary == NULL) return NULL;
    
	for (int i = 0; i < size; i++)
	{
	    if (strcmp(target, dictionary[i]) == 0)
	    {
	        return dictionary[i];
	    }
	}
	return NULL;
}